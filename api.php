<?php
/**
 *   API REST Naskot Maker
 *   Créateur: Julien JEAN
 *
 *
 *    Licence Creative Commons BY-SA 3.0 FR
 *    La licence est présente à la racine du projet
 *    Copyright (c) 2020 Julien JEAN
 *
 *  Merci d'installer composer !!!
 *  Ensuite pour installer le framework de base, composer require naskot/api-core
 *
 */

$security_include_identifier = 1; // Empêche l'utilisation des autres fichiers php sans le chargement de cette variable.
$time_app_exec = microtime(true); // debut du microtime pour <result_generated_in>
date_default_timezone_set('Europe/Paris'); // Timezone Paris

require_once 'config.env.php';
require_once 'vendor/autoload.php';

use Naskot\ApiCore;

new ApiCore();